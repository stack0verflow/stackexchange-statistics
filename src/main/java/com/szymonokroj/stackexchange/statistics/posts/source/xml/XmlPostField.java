package com.szymonokroj.stackexchange.statistics.posts.source.xml;

import com.szymonokroj.stackexchange.statistics.posts.source.PostsSourceException;

import java.time.LocalDateTime;
import java.util.function.Function;

class XmlPostField<T> {
    static final XmlPostField<Integer> ACCEPTED_ANSWER_ID = new XmlPostField<>(e -> e.getAttributeIntegerValue("AcceptedAnswerId").orElse(null));
    static final XmlPostField<LocalDateTime> CREATION_DATE = new XmlPostField<>(e -> e.getAttributeLocalDateTimeValue("CreationDate").orElseThrow(PostsSourceException::new));
    static final XmlPostField<Integer> SCORE = new XmlPostField<>(e -> e.getAttributeIntegerValue("Score").orElse(0));
    static final XmlPostField<Integer> COMMENT_COUNT = new XmlPostField<>(e -> e.getAttributeIntegerValue("CommentCount").orElse(0));

    private final Function<XMLStreamReaderWrapper, T> mapper;

    private XmlPostField(Function<XMLStreamReaderWrapper, T> mapper) {
        this.mapper = mapper;
    }

    T mapFrom(XMLStreamReaderWrapper reader) {
        return mapper.apply(reader);
    }
}
