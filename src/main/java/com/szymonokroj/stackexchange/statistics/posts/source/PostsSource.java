package com.szymonokroj.stackexchange.statistics.posts.source;

import com.szymonokroj.stackexchange.statistics.posts.model.Post;

import java.io.IOException;
import java.util.stream.Stream;

public interface PostsSource extends AutoCloseable {

    Stream<Post> streamPosts();

    void close() throws IOException;
}
