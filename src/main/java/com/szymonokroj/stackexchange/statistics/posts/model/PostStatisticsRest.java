package com.szymonokroj.stackexchange.statistics.posts.model;

import java.time.LocalDateTime;

public class PostStatisticsRest {

    private final LocalDateTime analyseDate;
    private final PostAnalyseDetails details;

    public PostStatisticsRest(LocalDateTime analyseDate, PostAnalyseDetails details) {
        this.analyseDate = analyseDate;
        this.details = details;
    }

    public LocalDateTime getAnalyseDate() {
        return analyseDate;
    }

    public PostAnalyseDetails getDetails() {
        return details;
    }
}
