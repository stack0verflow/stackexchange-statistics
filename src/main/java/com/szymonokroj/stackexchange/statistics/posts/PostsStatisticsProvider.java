package com.szymonokroj.stackexchange.statistics.posts;


import com.szymonokroj.stackexchange.statistics.posts.model.PostAnalyseDetails;
import com.szymonokroj.stackexchange.statistics.posts.model.PostStatisticsRest;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSource;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSourceException;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
class PostsStatisticsProvider {

    private final PostsSourceFactory postsSourceFactory;
    private final Supplier<LocalDateTime> localDateTimeSupplier;

    @Autowired
    PostsStatisticsProvider(PostsSourceFactory postsSourceFactory) {
        this(postsSourceFactory, LocalDateTime::now);
    }

    private PostsStatisticsProvider(PostsSourceFactory postsSourceFactory, Supplier<LocalDateTime> localDateTimeSupplier) {
        this.postsSourceFactory = postsSourceFactory;
        this.localDateTimeSupplier = localDateTimeSupplier;
    }

    PostStatisticsRest getPostsStatistics(URL url) {
        return getStatistics(postsSourceFactory.getSource(url));
    }

    private PostStatisticsRest getStatistics(PostsSource source) {
        try (source) {
            return source.streamPosts()
                    .map(PostAnalyseDetails::mapFromPost)
                    .collect(Collectors.collectingAndThen(
                            Collectors.reducing(new PostAnalyseDetails(), PostAnalyseDetails::merge),
                            r -> new PostStatisticsRest(localDateTimeSupplier.get(), r)));
        } catch (IOException e) {
            throw new PostsSourceException();
        }
    }
}
