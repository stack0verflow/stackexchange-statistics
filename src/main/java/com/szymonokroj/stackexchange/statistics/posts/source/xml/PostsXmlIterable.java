package com.szymonokroj.stackexchange.statistics.posts.source.xml;

import com.szymonokroj.stackexchange.statistics.posts.model.Post;

import javax.xml.stream.XMLStreamReader;
import java.util.Iterator;

class PostsXmlIterable implements Iterable<Post> {

    private final XMLStreamReader reader;

    PostsXmlIterable(XMLStreamReader reader) {
        this.reader = reader;
    }

    @Override
    public Iterator<Post> iterator() {
        return new PostsXmlReader(new XMLStreamReaderWrapper(reader));
    }
}

