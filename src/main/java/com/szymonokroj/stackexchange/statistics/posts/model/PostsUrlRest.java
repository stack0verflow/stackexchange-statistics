package com.szymonokroj.stackexchange.statistics.posts.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URL;

public class PostsUrlRest {

    private final URL url;

    @JsonCreator
    public PostsUrlRest(@JsonProperty("url") URL url) {
        this.url = url;
    }

    public URL getUrl() {
        return url;
    }
}
