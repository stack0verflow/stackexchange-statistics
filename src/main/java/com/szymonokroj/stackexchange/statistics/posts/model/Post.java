package com.szymonokroj.stackexchange.statistics.posts.model;

import java.time.LocalDateTime;
import java.util.Optional;

public class Post {

    private final Integer acceptedAnswerId;
    private final LocalDateTime creationDate;
    private final Integer score;
    private final Integer commentCount;

    public Post(Integer acceptedAnswerId, LocalDateTime creationDate, Integer score, Integer commentCount) {
        this.acceptedAnswerId = acceptedAnswerId;
        this.creationDate = creationDate;
        this.score = score;
        this.commentCount = commentCount;
    }

    public Optional<Integer> getAcceptedAnswerId() {
        return Optional.ofNullable(acceptedAnswerId);
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public Integer getScore() {
        return score;
    }

    public Integer getCommentCount() {
        return commentCount;
    }
}
