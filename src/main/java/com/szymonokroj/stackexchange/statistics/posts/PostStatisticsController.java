package com.szymonokroj.stackexchange.statistics.posts;

import com.szymonokroj.stackexchange.statistics.posts.model.PostStatisticsRest;
import com.szymonokroj.stackexchange.statistics.posts.model.PostsUrlRest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class PostStatisticsController {

    private final PostsStatisticsProvider postsStatisticsProvider;

    public PostStatisticsController(PostsStatisticsProvider postsStatisticsProvider) {
        this.postsStatisticsProvider = postsStatisticsProvider;
    }

    @PostMapping("analyse")
    public PostStatisticsRest getXmlStatistics(@RequestBody @Valid PostsUrlRest postsUrl) {
       return postsStatisticsProvider.getPostsStatistics(postsUrl.getUrl());
    }
}
