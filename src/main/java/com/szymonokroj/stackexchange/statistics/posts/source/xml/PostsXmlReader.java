package com.szymonokroj.stackexchange.statistics.posts.source.xml;

import com.szymonokroj.stackexchange.statistics.posts.model.Post;

import javax.xml.stream.XMLStreamConstants;
import java.util.Iterator;

class PostsXmlReader implements Iterator<Post> {

    private final XMLStreamReaderWrapper xmlStreamReader;

    PostsXmlReader(XMLStreamReaderWrapper xmlStreamReaderWrapper) {
        this.xmlStreamReader = xmlStreamReaderWrapper;
    }

    @Override
    public boolean hasNext() {
        return xmlStreamReader.hasNext() && moveToNextNode();
    }

    @Override
    public Post next() {
        return parsePage();
    }

    private boolean moveToNextNode() {
        var eventType = xmlStreamReader.next();
        while (isNotRowNode(eventType)) {
            if (!xmlStreamReader.hasNext()) {
                return false;
            }
            eventType = xmlStreamReader.next();
        }
        return true;
    }

    private Post parsePage() {
        return new Post(
                XmlPostField.ACCEPTED_ANSWER_ID.mapFrom(xmlStreamReader),
                XmlPostField.CREATION_DATE.mapFrom(xmlStreamReader),
                XmlPostField.SCORE.mapFrom(xmlStreamReader),
                XmlPostField.COMMENT_COUNT.mapFrom(xmlStreamReader));
    }

    private boolean isNotRowNode(int eventType) {
        return !(eventType == XMLStreamConstants.START_ELEMENT) || !xmlStreamReader.isCurrentlyOnRowNode();
    }
}
