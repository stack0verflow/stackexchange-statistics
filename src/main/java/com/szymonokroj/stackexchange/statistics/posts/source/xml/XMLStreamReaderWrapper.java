package com.szymonokroj.stackexchange.statistics.posts.source.xml;

import com.szymonokroj.stackexchange.statistics.posts.source.PostsSourceException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.time.LocalDateTime;
import java.util.Optional;

class XMLStreamReaderWrapper {

    private static final String POST_ROW_NAME = "row";

    private final XMLStreamReader xmlStreamReader;

    XMLStreamReaderWrapper(XMLStreamReader xmlStreamReader) {
        this.xmlStreamReader = xmlStreamReader;
    }

    boolean hasNext() {
        try {
            return xmlStreamReader.hasNext();
        } catch (XMLStreamException e) {
            throw new PostsSourceException();
        }
    }

    int next() {
        try {
            return xmlStreamReader.next();
        } catch (XMLStreamException e) {
            throw new PostsSourceException();
        }
    }

    boolean isCurrentlyOnRowNode() {
        return xmlStreamReader.getName().getLocalPart().equals(POST_ROW_NAME);
    }

    Optional<Integer> getAttributeIntegerValue(String attributeName) {
        return Optional.ofNullable(xmlStreamReader.getAttributeValue(null, attributeName)).map(Integer::valueOf);
    }

    Optional<LocalDateTime> getAttributeLocalDateTimeValue(String attributeName) {
        return Optional.ofNullable(xmlStreamReader.getAttributeValue(null, attributeName)).map(LocalDateTime::parse);
    }
}
