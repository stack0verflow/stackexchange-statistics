package com.szymonokroj.stackexchange.statistics.posts.source;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PostsSourceException extends RuntimeException {

    public PostsSourceException() {
        super("Provided source is invalid.");
    }
}
