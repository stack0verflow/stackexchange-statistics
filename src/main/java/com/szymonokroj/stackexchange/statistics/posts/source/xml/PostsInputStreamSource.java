package com.szymonokroj.stackexchange.statistics.posts.source.xml;

import com.szymonokroj.stackexchange.statistics.posts.model.Post;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSource;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSourceException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class PostsInputStreamSource implements PostsSource {

    private final XMLInputFactory xmlInputFactory;
    private final InputStream inputStream;

    public PostsInputStreamSource(XMLInputFactory xmlInputFactory, InputStream inputStream) {
        this.xmlInputFactory = xmlInputFactory;
        this.inputStream = inputStream;
    }

    @Override
    public Stream<Post> streamPosts() {
        try {
            var reader = xmlInputFactory.createXMLStreamReader(inputStream);
            var xmlReader = new PostsXmlIterable(reader);
            return StreamSupport.stream(xmlReader.spliterator(), false);
        } catch (XMLStreamException e) {
            throw new PostsSourceException();
        }
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
