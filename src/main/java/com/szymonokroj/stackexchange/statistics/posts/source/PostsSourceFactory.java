package com.szymonokroj.stackexchange.statistics.posts.source;

import com.szymonokroj.stackexchange.statistics.posts.source.xml.PostsInputStreamSource;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLInputFactory;
import java.io.IOException;
import java.net.URL;

@Service
public class PostsSourceFactory {

    private final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    public PostsSource getSource(URL url) {
        try {
            var inputStream = url.openStream();
            return new PostsInputStreamSource(xmlInputFactory, inputStream);
        } catch (IOException e) {
            throw new PostsSourceException();
        }
    }
}
