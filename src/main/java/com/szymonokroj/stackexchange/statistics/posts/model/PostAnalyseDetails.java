package com.szymonokroj.stackexchange.statistics.posts.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

public class PostAnalyseDetails {

    private final LocalDateTime firstPost;
    private final LocalDateTime lastPost;
    private final Long totalPosts;
    private final Long totalAcceptedPosts;
    private final BigDecimal avgScore;
    private final Long maxCommentCount;

    public PostAnalyseDetails(LocalDateTime firstPost, LocalDateTime lastPost, Long totalPosts, Long totalAcceptedPosts, BigDecimal avgScore, Long maxCommentCount) {
        this.firstPost = firstPost;
        this.lastPost = lastPost;
        this.totalPosts = totalPosts;
        this.totalAcceptedPosts = totalAcceptedPosts;
        this.avgScore = avgScore;
        this.maxCommentCount = maxCommentCount;
    }

    public PostAnalyseDetails() {
        firstPost = LocalDateTime.MAX;
        lastPost = LocalDateTime.MIN;
        totalPosts = 0L;
        totalAcceptedPosts = 0L;
        avgScore = BigDecimal.ZERO;
        maxCommentCount = 0L;
    }

    public static PostAnalyseDetails mapFromPost(Post p) {
        return new PostAnalyseDetails(
                p.getCreationDate(),
                p.getCreationDate(),
                1L,
                p.getAcceptedAnswerId().map(r -> 1L).orElse(0L),
                BigDecimal.valueOf(p.getScore()),
                p.getCommentCount().longValue());
    }

    public LocalDateTime getFirstPost() {
        return firstPost;
    }

    public LocalDateTime getLastPost() {
        return lastPost;
    }

    public Long getTotalPosts() {
        return totalPosts;
    }

    public Long getTotalAcceptedPosts() {
        return totalAcceptedPosts;
    }

    public BigDecimal getAvgScore() {
        return avgScore;
    }

    public Long getMaxCommentCount() {
        return maxCommentCount;
    }

    public PostAnalyseDetails merge(PostAnalyseDetails b) {
        var totalPosts = this.getTotalPosts() + b.getTotalPosts();
        var avgScore = countAverageScore(b, totalPosts);

        return new PostAnalyseDetails(
                this.getFirstPost().isBefore(b.getFirstPost()) ? this.getFirstPost() : b.getFirstPost(),
                this.getLastPost().isAfter(b.getLastPost()) ? this.getLastPost() : b.getLastPost(),
                totalPosts,
                this.getTotalAcceptedPosts() + b.getTotalAcceptedPosts(),
                avgScore,
                Long.max(this.getMaxCommentCount(), b.getMaxCommentCount()));
    }

    private BigDecimal countAverageScore(PostAnalyseDetails b, long totalPosts) {
        var currentScoreTimesCurrentPostsCount = this.getAvgScore().multiply(BigDecimal.valueOf(this.getTotalPosts()));
        var summedScore = currentScoreTimesCurrentPostsCount.add(b.getAvgScore());
        return summedScore.divide(BigDecimal.valueOf(totalPosts), 10, RoundingMode.HALF_UP);
    }
}
