package com.szymonokroj.stackexchange.statistics.posts;

import com.szymonokroj.stackexchange.statistics.posts.model.Post;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSource;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSourceException;
import com.szymonokroj.stackexchange.statistics.posts.source.PostsSourceFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PostsStatisticsProviderTest {

    @Mock
    private Supplier<LocalDateTime> localDateTimeSupplier;
    @Mock
    private PostsSource postsSource;
    @Mock
    private PostsSourceFactory postsSourceFactory;

    @InjectMocks
    private PostsStatisticsProvider postsStatisticsProvider;

    @DisplayName("Should provide posts statistics")
    @Test
    void providesStatistics() throws MalformedURLException {
        //arrange
        var url = new URL("https://example.com/");
        when(postsSourceFactory.getSource(url)).thenReturn(postsSource);
        when(postsSource.streamPosts()).thenReturn(streamExamplePosts());

        var now = LocalDateTime.now();
        when(localDateTimeSupplier.get()).thenReturn(now);

        //act
        var statistics = postsStatisticsProvider.getPostsStatistics(url);

        //assert
        assertThat(statistics.getAnalyseDate()).isEqualTo(now);

        var details = statistics.getDetails();
        assertThat(details.getFirstPost()).isEqualTo("2017-01-01T01:01:00");
        assertThat(details.getLastPost()).isEqualTo("2019-01-01T01:01:00");
        assertThat(details.getTotalPosts()).isEqualTo(2);
        assertThat(details.getTotalAcceptedPosts()).isEqualTo(1);
        assertThat(details.getAvgScore()).isEqualByComparingTo(BigDecimal.valueOf(6.5));
        assertThat(details.getMaxCommentCount()).isEqualTo(5);
    }

    @DisplayName("Should throw PostsSourceException if IOException is thrown")
    @Test
    void throwsPostsSourceException() throws IOException {
        //arrange
        var url = new URL("https://example.com/");
        when(postsSourceFactory.getSource(url)).thenReturn(postsSource);
        doThrow(new IOException()).when(postsSource).close();

        //act & assert
        assertThatThrownBy(() -> postsStatisticsProvider.getPostsStatistics(url)).isInstanceOf(PostsSourceException.class);
    }

    @DisplayName("Should close source")
    @Test
    void closesSource() throws IOException {
        //arrange
        var url = new URL("https://example.com/");
        when(postsSourceFactory.getSource(url)).thenReturn(postsSource);
        when(postsSource.streamPosts()).thenReturn(Stream.empty());

        //act
        postsStatisticsProvider.getPostsStatistics(url);

        //assert
        verify(postsSource).close();
    }

    private Stream<Post> streamExamplePosts() {
        return Stream.of(
                new Post(1, LocalDateTime.of(2019, 1, 1, 1, 1), 5, 5),
                new Post(null, LocalDateTime.of(2017, 1, 1, 1, 1), 8, 2)
        );
    }
}