package com.szymonokroj.stackexchange.statistics.posts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.http.ContentType;
import com.szymonokroj.stackexchange.statistics.posts.model.PostsUrlRest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.time.LocalDate;

import static com.jayway.restassured.RestAssured.given;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PostStatisticsControllerTest {

    private ClientAndServer mockServer;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("classpath:test-xml.xml")
    Resource testXml;

    @Value("classpath:malformed-xml.xml")
    Resource malformedXml;

    @BeforeEach
    void startMockServer() {
        mockServer = startClientAndServer(1080);
    }

    @AfterEach
    void stopMockServer() {
        mockServer.stop();
    }

    @DisplayName("Should provide posts statistics")
    @Test
    void providesStats(@LocalServerPort Integer port) throws IOException {
        //arrange
        mockServer.when(HttpRequest.request().withPath("/xml"))
                .respond(HttpResponse.response().withBody(Files.readAllBytes(testXml.getFile().toPath())));
        var urlRest = new PostsUrlRest(new URL("http://localhost:1080/xml"));

        //act
        given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(urlRest))
                .when()
                .post("/analyse")
                .prettyPeek()
                .then()
                .statusCode(200)
                .body("analyseDate", Matchers.startsWith(LocalDate.now().toString()))
                .body("details.firstPost", Matchers.equalTo("2016-01-12T18:45:19.963"))
                .body("details.lastPost", Matchers.equalTo("2016-03-04T13:30:22.41"))
                .body("details.totalPosts", Matchers.equalTo(655))
                .body("details.totalAcceptedPosts", Matchers.equalTo(102))
                .body("details.avgScore", Matchers.comparesEqualTo(3.2732825f))
                .body("details.maxCommentCount", Matchers.equalTo(15));
    }

    @DisplayName("Should return bad request if xml source is malformed")
    @Test
    void returnsBadRequestOnMalformedXml(@LocalServerPort Integer port) throws IOException {
        //arrange
        mockServer.when(HttpRequest.request().withPath("/xml"))
                .respond(HttpResponse.response().withBody(Files.readAllBytes(malformedXml.getFile().toPath())));
        var urlRest = new PostsUrlRest(new URL("http://localhost:1080/xml"));

        //act
        given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(urlRest))
                .when()
                .post("/analyse")
                .prettyPeek()
                .then()
                .statusCode(400)
                .body("message", Matchers.equalTo("Provided source is invalid."));
    }
}