package com.szymonokroj.stackexchange.statistics.posts.source.xml;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PostsXmlReaderTest {

    @Mock
    private XMLStreamReaderWrapper xmlStreamReader;

    @InjectMocks
    private PostsXmlReader postsXmlReader;

    @DisplayName("Should move to next node after hasNext call")
    @Test
    void movesToNextNode() {
        //arrange
        when(xmlStreamReader.hasNext()).thenReturn(true);
        when(xmlStreamReader.next()).thenReturn(2, 1, 1);
        when(xmlStreamReader.isCurrentlyOnRowNode()).thenReturn(false, true);

        //act
        postsXmlReader.hasNext();

        //assert
        verify(xmlStreamReader, times(3)).next();
    }

    @DisplayName("Should parse post after next call")
    @Test
    void parsesPost() {
        //arrange
        doReturn(Optional.of(1)).when(xmlStreamReader).getAttributeIntegerValue("Score");
        doReturn(Optional.of(1)).when(xmlStreamReader).getAttributeIntegerValue("CommentCount");
        doReturn(Optional.of(1)).when(xmlStreamReader).getAttributeIntegerValue("AcceptedAnswerId");

        when(xmlStreamReader.getAttributeLocalDateTimeValue("CreationDate")).thenReturn(Optional.of(LocalDateTime.of(2018, 1, 1, 1, 1)));

        //act
        var post = postsXmlReader.next();

        //assert
        assertThat(post.getCreationDate()).isEqualTo("2018-01-01T01:01:00");
        assertThat(post.getAcceptedAnswerId()).hasValue(1);
        assertThat(post.getCommentCount()).isEqualTo(1);
        assertThat(post.getScore()).isEqualTo(1);
    }
}