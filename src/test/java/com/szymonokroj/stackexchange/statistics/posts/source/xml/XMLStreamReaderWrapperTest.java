package com.szymonokroj.stackexchange.statistics.posts.source.xml;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class XMLStreamReaderWrapperTest {

    @Mock
    private XMLStreamReader xmlStreamReader;

    @InjectMocks
    private XMLStreamReaderWrapper xmlStreamReaderWrapper;

    @DisplayName("Should get attribute Integer value")
    @Test
    void getsIntValue() {
        //arrange
        when(xmlStreamReader.getAttributeValue(null, "Int")).thenReturn("1");

        //act
        var intValue = xmlStreamReaderWrapper.getAttributeIntegerValue("Int");

        //assert
        assertThat(intValue).hasValue(1);
    }

    @DisplayName("Should get attribute LocalDateTime value")
    @Test
    void getsLocalDateTimeValue() {
        //arrange
        when(xmlStreamReader.getAttributeValue(null, "Date")).thenReturn("2018-01-01T01:01:01");

        //act
        var intValue = xmlStreamReaderWrapper.getAttributeLocalDateTimeValue("Date");

        //assert
        assertThat(intValue).hasValue(LocalDateTime.of(2018, 1, 1, 1, 1, 1));
    }

    @DisplayName("Should return false if currently not on row node")
    @Test
    void notOnRowNode() {
        //arrange
        when(xmlStreamReader.getName()).thenReturn(new QName("notRow"));

        //act
        var currentlyOnRowNode = xmlStreamReaderWrapper.isCurrentlyOnRowNode();

        //assert
        assertThat(currentlyOnRowNode).isFalse();
    }

    @DisplayName("Should retuen true if currently on row node")
    @Test
    void onRowNode() {
        //arrange
        when(xmlStreamReader.getName()).thenReturn(new QName("row"));

        //act
        var currentlyOnRowNode = xmlStreamReaderWrapper.isCurrentlyOnRowNode();

        //assert
        assertThat(currentlyOnRowNode).isTrue();
    }
}